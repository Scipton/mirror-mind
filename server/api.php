<?php
$puzzlesPath = "data/puzzles.json";
$telebotparams = "data/telebot.json";
$key = json_decode(file_get_contents($telebotparams))->{'key'};
if(!isset($_POST['key']) || $_POST['key'] !== $key){
  echo "Authentication failed";
  return;
}

////////////////////////////////////////////////////////////////////////////////////////
//Добавление ссылок на звонки в пазл по дате
//$data:
//$href = "https://www.dropbox.com/home/puzzle/diary/call/02_Apr_2017";
//$date = "2017-05-21";
//$ancor = "телефонные разговоры";
////////////////////////////////////////////////////////////////////////////////////////
if(isset($_POST['type']) && $_POST['type'] == 'call-link'){
  if(!isset($_POST['href']) || !isset($_POST['date']) || !isset($_POST['ancor'])){
    echo "ддолжны быть в POST href, date, ancor";
    return;
  }
  addLinkToDiary($_POST['href'], $_POST['date'], $_POST['ancor']);
  echo "OK";
}

////////////////////////////////////////////////////////////////////////////////////////
//Получение настроек для телеграм бота
////////////////////////////////////////////////////////////////////////////////////////
if(isset($_POST['type']) && $_POST['type'] == "settings-bot"){
  echo getBotSettings();
}

////////////////////////////////////////////////////////////////////////////////////////
//Добавление ссылок на звонки в пазл по дате
//$data:
//$href = "https://www.dropbox.com/home/puzzle/diary/call/02_Apr_2017";
//$date = "2017-05-21";
//$ancor = "телефонные разговоры";
////////////////////////////////////////////////////////////////////////////////////////
if(isset($_POST['type']) && $_POST['type'] == 'add-by-button-id'){
  if(!isset($_POST['text']) || !isset($_POST['id'])){
    echo "ддолжны быть в POST text, id";
    return;
  }
  writeTextByButtonId($_POST['id'], $_POST['text']);
  echo "OK";
}

return;
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////ФУНКЦИИ//////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//Запись текста в соответствии с настройками кнопки в телеграм боте
function writeTextByButtonId($id, $text){
  global $telebotparams;
  global $puzzlesPath;
  $button = null;
  $settings = json_decode(file_get_contents($telebotparams));
  foreach ($settings->{'buttons'} as $lineButtons){
    foreach ($lineButtons as $b){
      if($b->{'id'} == $id){
        $button = $b;
      }
    }
  }
  $curTime = date("H:i:s");
  $timeText = '';
  if(isset($button->{'addTime'}) && $button->{'addTime'}){
    $timeText = "($curTime) ";
  }
  $text = nl2br($text);
  $puzzles = json_decode(file_get_contents($puzzlesPath));
  if($button->{'type'} == 'puzzle'){//Если добавляем в определенный пазл
    $puzzle = $puzzles->{$button->{'puzzle'}};
    $puzzle->{$button->{'placeWrite'}} .= writeTextFormat($timeText, $text);
    $textRes = json_encode($puzzles);
    if($textRes){
      file_put_contents($puzzlesPath, $textRes);
    }
  }else{//Если добавляем по тэгам в пазл с текущим днем или если его нет, создаем новый
    $puzzle = null;
    foreach ($puzzles as $e){
      if(!(count($e->{'tags'}) == count($button->{'tags'})))continue;//Совпадает ли количество тэгов в массиве кнопки и фвента
      $skip = false;
      foreach ($e->{'tags'} as $puzzletag){//Совпадают ли все тэги
        if(!in_array($puzzletag, $button->{'tags'})){
          $skip = true;
        }
      }
      if($skip)continue;
      $today = date("Y-m-d");
      if(substr_count($e->{'date'}, $today) == 1){
        $puzzle = $e;
      }
    }

    if(isset($puzzle)){//Если есть пазл с текущей датой.
      $puzzle->{$button->{'placeWrite'}} .= writeTextFormat($timeText, $text);
      $textRes = json_encode($puzzles);
      if($textRes){
        file_put_contents($puzzlesPath, $textRes);
      }
    }else{//Надо создать новый пазл на сегодня.
      $puzzle = (object)array(
        "name" => "auto (".date("j F").")",
        "description" => "",
        "moreDescription" => "",
        "remember" => 10,
        "significance" => 1,
        "date" => date("Y-m-d H:i:s"),
        "repeat" => date("Y-m-d H:i:s"),
        "start" => "",
        "end" => "",
        "tags" => $button->{'tags'}
      );
      $puzzle->{$button->{'placeWrite'}} .= writeTextFormat($timeText, $text);
      $added = addObjInList($puzzles, $puzzle);
      $puzzles = $added->{'list'};
      $textRes = json_encode($puzzles);
      if($textRes){
        file_put_contents($puzzlesPath, $textRes);
      }
    }
  }
}
//Возвращаегт настройки для телебота
function getBotSettings(){
  global $telebotparams;
  return file_get_contents($telebotparams);
}
//Добавление ссылки в эвент "Дневник" указанной даты, с определенным анкором
function addLinkToDiary($href, $date, $ancor){
  global $puzzlesPath;
  $puzzles =  json_decode(file_get_contents($puzzlesPath));
  foreach ($puzzles as $id=>$puzzle){
    $addtext ='<br><a href = "'.$href.'" target="_blank">'.$ancor.'</a>';
    if(in_array('15', $puzzle->{'tags'})
      && substr_count($puzzle->{'date'}, $date) == 1
      && count($puzzle->{'tags'}) == 1
      && substr_count($puzzle->{'description'}, $addtext) == 0 ){
      $puzzle->{'description'} .= $addtext;
    }
  }
  $textRes = json_encode($puzzles);
  if($textRes){
    file_put_contents($puzzlesPath, $textRes);
  }
}
//console.log))
function c($data){
  echo json_encode($data);
}
//Добавляет новый объект в массив(который является объектом), определяя максимальный id
function addObjInList($list, $obj){
  $idMax = 0;
  foreach($list as $id=>$val){
    if($id>$idMax){
      $idMax=$id;
    }
  }
  $idMax++;
  $list->{$idMax}=$obj;
  return (object)array(
    'id' => $idMax,
    'list' => $list
  );
}

//Запись текста в соответствии с форматом h1, h2 b, br.
function writeTextFormat($time, $text){
  if($text == "---"){
    return "<hr />";
  }
  if($text == "!*" ){//Обычный параграф
    return "</ul>";
  }
  if($text == "*!" ){//Обычный параграф
    return "<ul>";
  }
  if($text == "#!" ){//Обычный параграф
    return "<ol>";
  }
  if($text == "!#" ){//Обычный параграф
    return "</ol>";
  }
  if($text[0] == "!"){//Заголовок определенного уровня
    $i = 1;
    while(true){
      if(!isset($text[$i]))return "";
      if($text[$i] == "!"){
        $i++;
      }else{
        break;
      }
    }
    $text = str_replace("!", "", $text);
    return "<h$i>$text</h$i>";
  }
  if($text[0] == "*" || $text[0] == "#"){//Заголовок определенного уровня
    $text = substr($text, 1);
    $text = "<li>$text</li>";
  }
  else{
    $text = "<p>$time$text</p>";
  }
  //заменяем жирное
  while(true){
    $first = strpos($text, "@");
    if(strpos($text, "@") === false || strpos($text, "@", $first+1) === false)break;
    $text = replaceFirst("@", "<b>", $text);
    $text = replaceFirst("@", "</b>", $text);
  }
  return $text;
}
//Удалить символ в строке по номеру
function sliceStr($str, $i){
  return substr($str, 0, $i).substr($str, $i+1);
}
//Заменяет первое вхождение
function replaceFirst($c, $isert, $str){
  $i = strpos($str, $c);
  $str = sliceStr($str, $i);
  return substr($str, 0, $i).$isert.substr($str, $i);
}