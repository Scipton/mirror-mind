<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
require 'vendor/autoload.php';
$app = new \Slim\App;
$container = $app->getContainer();
$container['path'] = 'data/';//Данные тут

////////////////////////////////SETTINGS//////////////////////////////////////
//Начальныее настройки
$app->get('/settings', function () {
  //1. Все фастфильтры
  $filters = json_decode(file_get_contents($this->path."filters.json"));
  //2. Все тэги
  $tagsArr = array();
  $tags = json_decode(file_get_contents($this->path."tags.json"));
  foreach($tags as $id=>$tag){
    array_push($tagsArr, (object)array(
      "id" => $id,
      "name" => $tag->{'name'}
    ));
  }
  //3. Свойства по которым можно сортировать
  $sortPropArr = json_decode(file_get_contents($this->path."sortProperties.json"));
  //Итоговые данные
  $settings = (object)array(
    "tags" => $tagsArr,
    "sort_properties" => $sortPropArr,
    "fast_filters" => $filters
  );
  echo json_encode($settings);
  return;
});
//Сохранить тэг
$app->post('/save-tag', function (Request $request, Response $response) {
  $tag =  json_decode($request->getParsedBody()['tag']);
  $id = $tag->{'id'};
  $tags = json_decode(file_get_contents($this->path."tags.json"));
  $name = $tag->{'name'};
  if($id == '-1'){//создание нового
    $obj = (object)array(
      'name' => $name,
      'children' => array()
    );
    $res = addObjInList($tags, $obj);
    file_put_contents($this->path."tags.json", json_encode($res->{'list'}));
    echo $res->{'id'};
  }else{//Изменение
    $tags->{$id}->{'name'} = $name;
    file_put_contents($this->path."tags.json", json_encode($tags));
    echo "OK";
  }
});
//Удалить тэг
$app->post('/delete-tag', function (Request $request, Response $response) {
  $tag =  json_decode($request->getParsedBody()['tag']);
  $id = $tag->{'id'};
  $tags = json_decode(file_get_contents($this->path."tags.json"));
  $name = $tag->{'name'};
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $result = array();
  foreach ($puzzles as $puzzle){
    if(in_array($id, $puzzle->{'tags'})){
      echo "can not delete";
      return;
    }
  }
  unset($tags->{$id});
  file_put_contents($this->path."tags.json", json_encode($tags));
  echo "OK";
});


////////////////////////////////PUZZLES//////////////////////////////////////
//Получить евенты по фильтру
$app->post('/puzzles', function (Request $request, Response $response) {
  $filter = json_decode($request->getParsedBody()['filter']);
  $tags = $filter->{'tags'};
  $allIn = $filter->{'allIn'};
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $result = array();
  foreach ($puzzles as $id=>$puzzle){
    if(existArrayInAnother($tags, $puzzle->{'tags'}, $allIn) || count($tags)==0){
      $puzzle->{'id'} = $id;
      array_push($result, $puzzle);
    }
  }
  $response = (object)array(
    'info' => 123,
    'puzzles' => $result
  );
  echo json_encode($response);
});
//Получить евент по айдишнику
$app->get('/puzzle/{id}', function (Request $request) {
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $id = $request->getAttributes()['id'];
  if(!isset($puzzles->{$id})){
    echo "no";
    return;
  }
  $puzzle = $puzzles->{$id};

  $puzzle->{'id'} = $id;
  echo json_encode($puzzle);
});
//Сохранить эвент или добавить новый
$app->post('/save-puzzle', function (Request $request, Response $response) {  
  $puzzle = json_decode($request->getParsedBody()['puzzle']);
  if(isset($puzzle->{'tmp'}->{'new'})){
    $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
    unset($puzzle->{'tmp'});
    $added = addObjInList($puzzles, $puzzle);
    file_put_contents($this->path."puzzles.json",json_encode($added->{'list'}));
    echo $added->{'id'};
  }else{
    $id = $puzzle->{'id'};
    unset($puzzle->{'id'});
    unset($puzzle->{'tmp'});
    $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
    $puzzles->{$id} = $puzzle;
    file_put_contents($this->path."puzzles.json", json_encode($puzzles));
    echo 'OK';
  }
  
});
//Сохранить новый фильтр
$app->post('/new-filter', function (Request $request, Response $response) {
  function nextId($arr){
    $id = -1;
    foreach($arr as $item){
      if((int)$item->{'id'} > $id){
        $id = (int)$item->{'id'};
      }
    }
    $id++;
    return $id;
  }
  $filter = json_decode($request->getParsedBody()['filter']);
  $filters = json_decode(file_get_contents($this->path."filters.json"));
  $id = nextId($filters);
  $filter->{'id'} = $id;
  array_push($filters, $filter);
  file_put_contents($this->path."filters.json", json_encode($filters));
  echo 'OK';
});
//Удалить фильтр по айдишнику
$app->post('/delete-filter', function (Request $request, Response $response) {
  $id = json_decode($request->getParsedBody()['id']);
  $filters = json_decode(file_get_contents($this->path."filters.json"));
  $result = array();
  foreach($filters as $filter){
    if($filter->{'id'} != $id){
      array_push($result, $filter);
    }
  }
  file_put_contents($this->path."filters.json", json_encode($result));
  echo 'OK';
});
//Удалить пазл
$app->post('/delete-puzzle', function (Request $request, Response $response) {
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $id =  $request->getParsedBody()['id'];
  unset($puzzles->{$id});
  file_put_contents($this->path."puzzles.json", json_encode($puzzles));
  echo "OK";
});
//Создать новый пазл по именам некоторых
$app->post('/new-by-some', function (Request $request, Response $response) {
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $ids =  $request->getParsedBody()['ids'];
  $puzzle = newPuzzle();
  $puzzle->{'description'} = "<p>";
  foreach($ids as $id){
    $today = (new DateTime($puzzles->{$id}->{'date'}));
    $puzzle->{'description'} .= "(".dayweek($today->format('D'))." ".$today->format('j')." ".monthRus($today->format('M')).") ";
    $name = $puzzles->{$id}->{'name'};
    $name = str_replace("<p>", "", $name);
    $name = str_replace("</p>", "", $name);
    $puzzle->{'description'} .= $name."<br>";
    foreach($puzzles->{$id}->{'tags'} as $tag){
      if(!in_array($tag, $puzzle->{'tags'})){
        array_push($puzzle->{'tags'}, $tag);
      }
    }
  }
  $puzzle->{'description'} .= "</p>";
  $res = addObjInList($puzzles, $puzzle);
  file_put_contents($this->path."puzzles.json", json_encode($res->{'list'}));
  echo $res->{'id'};
});
//Посиск пазлов
$app->post('/search', function (Request $request, Response $response) {
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $words =  $request->getParsedBody()['words'];
  $resPuzzles = array();
  foreach($puzzles as $id=>$puzzle){
    $textPuzzle = $puzzle->{'name'}.$puzzle->{'description'}.$puzzle->{'moreDescription'};
    if(allWordsInText($words,$textPuzzle)){
      $puzzle->{'id'} = $id;
      array_push($resPuzzles, $puzzle);
    }
  }
  c($resPuzzles);
});
//Посиск пазлов
$app->post('/upload-img', function (Request $request, Response $response) {
  $name = (new DateTime())->format('Y-m-d-H-i-s').$_FILES['upload']['name'];
  file_put_contents($this->path."img/".$name, file_get_contents($_FILES['upload']['tmp_name']));
  $res = (object)array(
    "uploaded" => 1,
    "fileName" => $name,
    "url" => "/server/data/img/".$name
  );
  echo json_encode($res);
});

////////////////////////////////TELEBOT///////////////////////////////////////
//НЛастройки телеграм
$app->get('/telebot-info', function () {
  $tags = json_decode(file_get_contents($this->path."tags.json"));
  $botinfo = json_decode(file_get_contents($this->path."telebot.json"));
  $res = (object)array(
    "botinfo" => $botinfo,
    "tags" => $tags
  );
  echo json_encode($res);
});
//Сохранить настройки телебота
$app->post('/save-telebot-info', function (Request $request, Response $response) {
  $info =  $request->getParsedBody()['info'];
  file_put_contents($this->path."telebot.json", json_encode($info));
  echo "OK";
});
//Получить чисто имена пазлов и их айдишники
$app->post('/fast-puzzles', function (Request $request, Response $response) {
  $puzzles = json_decode(file_get_contents($this->path."puzzles.json"));
  $tags =  $request->getParsedBody()['tags'];
  $res = array();
  foreach($puzzles as $id=>$puzzle){
    foreach($tags as $tagID){
      if(in_array($tagID, $puzzle->{'tags'})){
        array_push($res, (object)array(
          "id" => $id,
          "name" => $puzzle->{'name'}
        ));
        break;
      }
    }
  }
  echo json_encode($res);
});


////////////////////////////////MANTRAS///////////////////////////////////////
//Отдает последний отредактированный М
$app->get('/get-reflectors', function () {
  $listM = json_decode(file_get_contents($this->path."reflectors.json"));
  if(count($listM)==0){
    $newReflector = (object)array(
      "dateStart" => (new DateTime())->format('Y-m-d H:i:s'),
      "dateEnd" => "now",
      "mantras" => array()
    );
    array_push($listM, $newReflector);
    file_put_contents($this->path."reflectors.json", json_encode($listM));
    echo json_encode($newReflector);
    return;
  }
  foreach ($listM as $m){
    if($m->{'dateEnd'}=='now'){
      echo json_encode($m);
      return;
    }
  }
});
//Сохранение только рэйтинга
$app->post('/save-reflector', function (Request $request, Response $response) {
  $newM =$request->getParsedBody()['reflector'];
  $listM = json_decode(file_get_contents($this->path."reflectors.json"));
  foreach ($listM as $i=>$m){
    if($m->{'dateEnd'}=='now'){
      $listM[$i] = $newM;
      break;
    }
  }
  file_put_contents($this->path."reflectors.json", json_encode($listM));
  echo 'OK';
  return;
});
//Сохранение нового рефлектора
$app->post('/save-new-reflector', function (Request $request, Response $response) {
  $newM = $request->getParsedBody()['newReflector'];
  $listM = json_decode(file_get_contents($this->path."reflectors.json"));
  foreach ($listM as $i=>$m){
    if($m->{'dateEnd'}=='now'){
      $listM[$i]->{'dateEnd'} = $newM['dateStart'];
      break;
    }
  }
  $newM['dateStart'] = (new DateTime())->format('Y-m-d H:i:s');
  $newM['dateEnd'] = "now";
  array_unshift($listM, $newM);
  file_put_contents($this->path."reflectors.json", json_encode($listM));
  echo 'OK';
});
//Отдает статистику (хронологию изменений) в виде
$app->get('/reflectors-history', function () {
  $listM = json_decode(file_get_contents($this->path."reflectors.json"));
  $changes = array();
  for($i = 0; $i < count($listM)-1; $i++){
    array_push($changes, getDifference($listM[$i], $listM[$i+1]));
  }
  echo json_encode($changes);
});


////////////////////////ВСПОМОГАТЕЛЬНЫЕ ФУНКЦИИ///////////////////////////////
//Существуют ли элементы массива 1 во втором (all = true все должны существовать) (false - хотябы один, true - все)
function existArrayInAnother($needArr, $arr, $all){
  foreach($needArr as $needItem){
    $find = false;
    foreach($arr as $item){
      if($needItem == $item){
        $find = true;
        if(!$all){
          return true;
        }
      }
    }
    if(!$find && $all){
      return false;
    }
  }
  if(!$all){
    return false;
  }else{
    return true;
  }
}
//Добавляет новый объект в массив(который является объектом), определяя максимальный id
function addObjInList($list, $obj){
  $idMax = 0;
  foreach($list as $id=>$val){
    if($id>$idMax){
      $idMax=$id;
    }
  }
  $idMax++;
  $list->{$idMax}=$obj;
  return (object)array(
    'id' => $idMax,
    'list' => $list
  );
}
//echo
function c($data){
  echo json_encode($data);
}
//Создает новый пазл
function newPuzzle(){
  return (object)array(
    "name" => "auto (".(new DateTime())->format('j M').")",
    "date" => (new DateTime())->format('Y-m-d H:i:s'),
    "description" => "",
    "moreDescription" => "",
    "remember" => 10,
    "significance" => 1,
    "end" => "",
    "start" => "",
    "repeat" => (new DateTime())->format('Y-m-d H:i:s'),
    "tags" => array()
  );
}
//Русификатор недель
function dayweek($day){
  $days = (object)array(
    "Mon" => "Пн",
    "Tue" => "Вт",
    "Wed" => "Ср",
    "Thu" => "Чт",
    "Fri" => "Пт",
    "Sat" => "Сб",
    "Sun" => "Вс"
  );
  return $days->{$day};
}
//Русификатор месяцев
function monthRus($month){
  $months = (object)array(
    "Jan" => "Янв",
    "Feb" => "Фев",
    "Mar" => "Март",
    "Apr" => "Апр",
    "May" => "Май",
    "Jun" => "Июнь",
    "Jul" => "Июль",
    "Aug" => "Авг",
    "Sept" => "Сен",
    "Oct" => "Окт",
    "Nov" => "Ноя",
    "Dec" => "Дек"
  );
  return $months->{$month};
}
//Проверяет все ли слова есть в тексте (регистронезависимый)
function allWordsInText($words, $textPuzzle){
  $textPuzzle = mb_strtolower($textPuzzle, 'UTF-8');
  foreach($words as $word){
    $word = mb_strtolower($word, 'UTF-8');
    if(strripos($textPuzzle, $word) === false){//Если не найдена
      return false;
    }
  }
  return true;
}
//Взвращает различие между рефлекторами
function getDifference($mNew, $mOld){
  //Добавленные М
  $createdArr = array();
  //Удаленные М
  $deletedArr = array();
  //Измененные
  $updatedArr = array();
  //индексы М новых и старых
  $arrIndexNew = array();
  $arrIndexOld = array();
  foreach ($mNew->{'mantras'} as $mN){
    array_push($arrIndexNew, $mN->{'id'});
  }
  foreach ($mOld->{'mantras'} as $mO){
    array_push($arrIndexOld, $mO->{'id'});
  }
  ///Новые Id-шники и стаые
  $createdArrIds = array_diff($arrIndexNew, $arrIndexOld);
  $deletedArrIds = array_diff($arrIndexOld, $arrIndexNew);
  foreach ($mNew->{'mantras'} as $mN){
    if(in_array($mN->{'id'}, $createdArrIds)){
      array_push($createdArr, $mN);
    }
  }
  foreach ($mOld->{'mantras'} as $mO){
    if(in_array($mO->{'id'}, $deletedArrIds)){
      array_push($deletedArr, $mO);
    }
  }
  //Заполнение имененных М
  foreach ($mNew->{'mantras'} as $mN){
    //Если это новая, проходим дальше (ищем именно имененные)
    if(in_array($mN->{'id'}, $createdArrIds))continue;
    foreach ($mOld->{'mantras'} as $mO){
      //Если удаленный М продолжаем дальще
      if(in_array($mO->{'id'}, $deletedArrIds))continue;
      if($mO->{'id'} == $mN->{'id'} && !eq($mN, $mO)){
        $changedM = (object)array('text' => $mN->{'text'});
        if($mO->{'text'} != $mN->{'text'}){
          $changedM->{'oldText'} = $mO->{'text'};
        }
        //Записываем новые, старые и измененные айтемы.
        $oldItems = array();
        $oldItemsIds = array();
        $newItems = array();
        $newItemsIds = array();
        if(isset($mO->{'items'})){
          foreach ($mO->{'items'} as $item){
            array_push($oldItems, $item);
            array_push($oldItemsIds, $item->{'id'});
          }
        }
        if(isset($mN->{'items'})){
          foreach ($mN->{'items'} as $item){
            array_push($newItems, $item);
            array_push($newItemsIds, $item->{'id'});
          }
        }
        //Id - шники созданных и удаленных айтемов
        $createdItemsIds = array_diff($newItemsIds, $oldItemsIds);
        $deletedItemsIds = array_diff($oldItemsIds, $newItemsIds);
        //Заполнили new и old items
        if($createdItemsIds){
          $changedM->{'newItems'} = array();
          foreach ($mN->{'items'} as $item){
            if(in_array($item->{'id'}, $createdItemsIds)){
              array_push($changedM->{'newItems'}, $item->{'text'});
            }
          }
        }
        if($deletedItemsIds){
          $changedM->{'oldItems'} = array();
          foreach ($mO->{'items'} as $item){
            if(in_array($item->{'id'}, $deletedItemsIds)){
              array_push($changedM->{'oldItems'}, $item->{'text'});
            }
          }
        }
        //Если есть Item и там и там ищем измененные
        if(isset($mO->{'items'}) && isset($mN->{'items'})){
          foreach ($mN->{'items'} as $itemNew){
            if(in_array($itemNew->{'id'}, $createdItemsIds))continue;
            foreach ($mO->{'items'} as $itemOld){
              if(in_array($itemOld->{'id'}, $deletedItemsIds))continue;
              if($itemOld->{'id'} == $itemNew->{'id'} && !eq($itemNew, $itemOld)){
                if(!isset($changedM->{'changedItems'})){
                  $changedM->{'changedItems'} = array();
                }
                array_push($changedM->{'changedItems'}, (object)array(
                  'oldTextItem' => $itemOld->{'text'},
                  'newTextItem' => $itemNew->{'text'}
                ));
              }
            }
          }
        }
        array_push($updatedArr,$changedM);
        break;
      }
    }
  }

  $res = (object)array(
    'date' => $mNew->{'dateStart'},
    'createdM' => array(),
    'deletedM' => array(),
    'changedM' => $updatedArr,
  );
  //Заполнкние новых М
  if ($createdArr){
    foreach ($createdArr as $mN){
      $cratedM = (object)array('text' => $mN->{'text'});
      if(isset($mN->{'items'})){
        $cratedM->{'newItems'} = array();
        foreach ($mN->{'items'} as $item){
          array_push($cratedM->{'newItems'}, $item->{'text'});
        }
      }
      array_push($res->{'createdM'}, $cratedM);
    }
  }
  //Заполнкние удаленных М
  if ($deletedArr){
    foreach ($deletedArr as $mO){
      $deletedM = (object)array('oldText' => $mO->{'text'});
      if(isset($mO->{'items'})){
        $deletedM->{'oldItems'} = array();
        foreach ($mO->{'items'} as $item){
          array_push($deletedM->{'oldItems'}, $item->{'text'});
        }
      }
    }
    array_push($res->{'deletedM'}, $deletedM);
  }
  return $res;
}
//Сравнение на содержимое объектов
function eq($obj1, $obj2){
  return json_encode($obj1)==json_encode($obj2);
}


$app->run();