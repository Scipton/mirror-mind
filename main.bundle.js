webpackJsonp([7,10],{

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_menu_items_menu_items__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminLayoutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminLayoutComponent = (function () {
    function AdminLayoutComponent(router, menuItems, translate) {
        this.router = router;
        this.menuItems = menuItems;
        this.translate = translate;
        this.today = Date.now();
        this.showSettings = false;
        this.currentLang = 'en';
        this.root = 'ltr';
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        var elemSidebar = document.querySelector('.app-inner > .sidebar-panel');
        var elemContent = document.querySelector('.app-inner > .mat-sidenav-content');
        this.dark = true;
        this.boxed = false;
        this.collapseSidebar = false;
        this.compactSidebar = true;
        if (window.matchMedia("(min-width: 960px)").matches && !this.isMac() && !this.compactSidebar) {
            __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__["initialize"](elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
            __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__["initialize"](elemContent, { wheelSpeed: 2, suppressScrollX: true });
        }
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* NavigationEnd */]; }).subscribe(function (event) {
            _this.url = event.url;
            if (_this.isOver()) {
                _this.sidemenu.close();
            }
            if (window.matchMedia("(min-width: 960px)").matches && !_this.isMac() && !_this.compactSidebar) {
                __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__["update"](elemContent);
            }
        });
    };
    AdminLayoutComponent.prototype.onClick = function (e) {
        var _this = this;
        var elemSidebar = document.querySelector('.app-inner > .sidebar-panel');
        setTimeout(function () {
            if (window.matchMedia("(min-width: 960px)").matches && !_this.isMac() && !_this.compactSidebar) {
                __WEBPACK_IMPORTED_MODULE_4_perfect_scrollbar__["update"](elemSidebar);
            }
        }, 350);
    };
    AdminLayoutComponent.prototype.ngOnDestroy = function () {
        this._router.unsubscribe();
    };
    AdminLayoutComponent.prototype.isOver = function () {
        if (this.url === '/apps/messages' || this.url === '/apps/calendar' || this.url === '/apps/media' || this.url === '/maps/leaflet') {
            return true;
        }
        else {
            return window.matchMedia("(max-width: 960px)").matches;
        }
    };
    AdminLayoutComponent.prototype.isMac = function () {
        var bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    };
    AdminLayoutComponent.prototype.menuMouseOver = function () {
        if (window.matchMedia("(min-width: 960px)").matches && this.collapseSidebar) {
            this.sidemenu.mode = 'over';
        }
    };
    AdminLayoutComponent.prototype.menuMouseOut = function () {
        if (window.matchMedia("(min-width: 960px)").matches && this.collapseSidebar) {
            this.sidemenu.mode = 'side';
        }
    };
    AdminLayoutComponent.prototype.closeSettings = function () {
        this.showSettings = false;
        var obj = {
            dark: this.dark,
            boxed: this.boxed,
            collapseSidebar: this.collapseSidebar,
            compactSidebar: this.compactSidebar
        };
    };
    AdminLayoutComponent.prototype.addMenuItem = function () {
        this.menuItems.add({
            state: 'menu',
            name: 'MENU',
            type: 'sub',
            icon: 'trending_flat',
            children: [
                { state: 'menu', name: 'MENU' },
                { state: 'timelmenuine', name: 'MENU' }
            ]
        });
    };
    return AdminLayoutComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('sidemenu'),
    __metadata("design:type", Object)
], AdminLayoutComponent.prototype, "sidemenu", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AdminLayoutComponent.prototype, "onClick", null);
AdminLayoutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-layout',
        template: __webpack_require__(442)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__shared_menu_items_menu_items__["a" /* MenuItems */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__shared_menu_items_menu_items__["a" /* MenuItems */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__["d" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3_ng2_translate_ng2_translate__["d" /* TranslateService */]) === "function" && _c || Object])
], AdminLayoutComponent);

var _a, _b, _c;
//# sourceMappingURL=admin-layout.component.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthLayoutComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AuthLayoutComponent = (function () {
    function AuthLayoutComponent() {
    }
    return AuthLayoutComponent;
}());
AuthLayoutComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-layout',
        styles: [':host /deep/ .mat-sidenav-content {padding: 0;} .mat-sidenav-container {z-index: 1000}'],
        template: __webpack_require__(443)
    })
], AuthLayoutComponent);

//# sourceMappingURL=auth-layout.component.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(82);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AccordionDirective = (function () {
    function AccordionDirective(router) {
        this.router = router;
        this.navlinks = [];
    }
    AccordionDirective.prototype.closeOtherLinks = function (openLink) {
        this.navlinks.forEach(function (link) {
            if (link !== openLink) {
                link.open = false;
            }
        });
    };
    AccordionDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    AccordionDirective.prototype.removeGroup = function (link) {
        var index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    };
    AccordionDirective.prototype.getUrl = function () {
        return this.router.url;
    };
    AccordionDirective.prototype.ngOnInit = function () {
        var _this = this;
        this._router = this.router.events.filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* NavigationEnd */]; }).subscribe(function (event) {
            _this.navlinks.forEach(function (link) {
                if (link.group) {
                    var routeUrl = _this.getUrl();
                    var currentUrl = routeUrl.split('/');
                    if (currentUrl.indexOf(link.group) > 0) {
                        link.open = true;
                        _this.closeOtherLinks(link);
                    }
                }
            });
        });
    };
    return AccordionDirective;
}());
AccordionDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[appAccordion]',
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object])
], AccordionDirective);

var _a;
//# sourceMappingURL=accordion.directive.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordion_directive__ = __webpack_require__(136);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionLinkDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionLinkDirective = (function () {
    function AccordionLinkDirective(nav) {
        this.nav = nav;
    }
    Object.defineProperty(AccordionLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
            if (value) {
                this.nav.closeOtherLinks(this);
            }
        },
        enumerable: true,
        configurable: true
    });
    AccordionLinkDirective.prototype.ngOnInit = function () {
        this.nav.addLink(this);
    };
    AccordionLinkDirective.prototype.ngOnDestroy = function () {
        this.nav.removeGroup(this);
    };
    AccordionLinkDirective.prototype.toggle = function () {
        this.open = !this.open;
    };
    return AccordionLinkDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Object)
], AccordionLinkDirective.prototype, "group", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostBinding"])('class.open'),
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], AccordionLinkDirective.prototype, "open", null);
AccordionLinkDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[appAccordionLink]'
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordion_directive__["a" /* AccordionDirective */]) === "function" && _a || Object])
], AccordionLinkDirective);

var _a;
//# sourceMappingURL=accordionlink.directive.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuItems; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var MENUITEMS = [
    {
        state: '',
        name: 'Puzzles',
        type: 'link',
        icon: 'toc'
    },
    {
        state: 'tags',
        name: 'Tags',
        type: 'link',
        icon: 'apps'
    },
    {
        state: 'reflectors',
        name: 'Reflectors',
        type: 'link',
        icon: 'blur_on'
    },
    {
        state: 'telebot-settings',
        name: 'Telebot',
        type: 'link',
        icon: 'near_me'
    }
    //,
    // {
    //   state: 'home',
    //   name: 'HOME',
    //   type: 'link',
    //   icon: 'explore'
    // },
    // {
    //   state: 'apps',
    //   name: 'APPS',
    //   type: 'sub',
    //   icon: 'apps',
    //   badge: [
    //     {type: 'red', value: '5'}
    //   ],
    //   children: [
    //     {state: 'calendar', name: 'CALENDAR'},
    //     {state: 'media', name: 'MEDIA'},
    //     {state: 'messages', name: 'MESSAGES'},
    //     {state: 'social', name: 'SOCIAL'},
    //     {state: 'chat', name: 'CHAT'}
    //   ]
    // },
    // {
    //   state: 'widgets',
    //   name: 'WIDGETS',
    //   type: 'link',
    //   icon: 'photo'
    // },
    // {
    //   state: 'material',
    //   name: 'MATERIAL',
    //   type: 'sub',
    //   icon: 'equalizer',
    //   badge: [
    //     {type: 'purple', value: '10'}
    //   ],
    //   children: [
    //     {state: 'button', name: 'BUTTON'},
    //     {state: 'cards', name: 'CARDS'},
    //     {state: 'select', name: 'SELECT'},
    //     {state: 'autocomplete', name: 'AUTOCOMPLETE'},
    //     {state: 'chips', name: 'CHIPS'},
    //     {state: 'input', name: 'INPUT'},
    //     {state: 'checkbox', name: 'CHECKBOX'},
    //     {state: 'radio', name: 'RADIO'},
    //     {state: 'toolbar', name: 'TOOLBAR'},
    //     {state: 'lists', name: 'LISTS'},
    //     {state: 'grid', name: 'GRID'},
    //     {state: 'progress', name: 'PROGRESS'},
    //     {state: 'tabs', name: 'TABS'},
    //     {state: 'switch', name: 'SWITCH'},
    //     {state: 'tooltip', name: 'TOOLTIP'},
    //     {state: 'menu', name: 'MENU'},
    //     {state: 'slider', name: 'SLIDER'},
    //     {state: 'snackbar', name: 'SNACKBAR'},
    //     {state: 'dialog', name: 'DIALOG'}
    //   ]
    // },
    // {
    //   state: 'forms',
    //   name: 'FORMS',
    //   type: 'sub',
    //   icon: 'looks_3',
    //   children: [
    //     {state: 'editor', name: 'EDITOR'},
    //     {state: 'validation', name: 'VALIDATION'},
    //     {state: 'upload', name: 'UPLOAD'},
    //     {state: 'tree', name: 'TREE'},
    //   ]
    // },
    // {
    //   state: 'tables',
    //   name: 'TABLES',
    //   type: 'sub',
    //   icon: 'format_line_spacing',
    //   badge: [
    //     {type: 'blue-grey', value: '8'
    //     }
    //   ],
    //   children: [
    //     {state: 'fullscreen', name: 'FULLSCREEN'},
    //     {state: 'editing', name: 'EDITING'},
    //     {state: 'filter', name: 'FILTER'},
    //     {state: 'paging', name: 'PAGING'},
    //     {state: 'sorting', name: 'SORTING'},
    //     {state: 'pinning', name: 'PINNING'},
    //     {state: 'selection', name: 'SELECTION'},
    //   ]
    // },
    // {
    //   state: 'charts',
    //   name: 'CHARTS',
    //   type: 'link',
    //   icon: 'show_chart',
    // },
    // {
    //   state: 'maps',
    //   name: 'MAPS',
    //   type: 'sub',
    //   icon: 'navigation',
    //   badge: [
    //     {type: 'green', value: 'new'
    //     }
    //   ],
    //   children: [
    //     {state: 'google', name: 'GOOGLE'},
    //     {state: 'leaflet', name: 'LEAFLET'}
    //   ]
    // },
    // {
    //   state: 'dragndrop',
    //   name: 'DND',
    //   type: 'link',
    //   icon: 'show_chart',
    // },
    // {
    //   state: 'pages',
    //   name: 'PAGES',
    //   type: 'sub',
    //   icon: 'pages',
    //   children: [
    //     {state: 'invoice', name: 'INVOICE'},
    //     {state: 'timeline', name: 'TIMELINE'},
    //     {state: 'user', name: 'USER'},
    //     {state: 'blank', name: 'BLANK'},
    //     {state: 'pricing', name: 'PRICING'},
    //   ]
    // },
    // {
    //   state: 'session',
    //   name: 'SESSION',
    //   type: 'sub',
    //   icon: 'face',
    //   children: [
    //     {state: '404', name: '404'},
    //     {state: 'error', name: 'ERROR'},
    //     {state: 'signin', name: 'SIGNIN'},
    //     {state: 'signup', name: 'SIGNUP'},
    //     {state: 'forgot', name: 'FORGOT'},
    //     {state: 'lockscreen', name: 'LOCKSCREEN'},
    //   ]
    // },
    // {
    //   state: 'http://primer.nyasha.me/docs',
    //   name: 'DOCS',
    //   type: 'extTabLink',
    //   icon: 'local_library'
    // }
];
var MenuItems = (function () {
    function MenuItems() {
    }
    MenuItems.prototype.getAll = function () {
        return MENUITEMS;
    };
    MenuItems.prototype.add = function (menu) {
        MENUITEMS.push(menu);
    };
    return MenuItems;
}());
MenuItems = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
], MenuItems);

//# sourceMappingURL=menu-items.js.map

/***/ }),

/***/ 200:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./session/session.module": [
		513,
		0
	],
	"./zComponents/puzzles/puzzles.module": [
		514,
		1
	],
	"./zComponents/reflectors/reflectors.module": [
		515,
		4
	],
	"./zComponents/tags/tags.module": [
		516,
		3
	],
	"./zComponents/telebot/telebot.module": [
		517,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 200;


/***/ }),

/***/ 201:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(248);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_translate_ng2_translate__ = __webpack_require__(107);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent(translate) {
        translate.addLangs(['en', 'fr']);
        translate.setDefaultLang('en');
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: '<router-outlet></router-outlet>'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ng2_translate_ng2_translate__["d" /* TranslateService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ng2_translate_ng2_translate__["d" /* TranslateService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(32);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_material__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_flex_layout__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_routing__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__layouts_admin_admin_layout_component__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__layouts_auth_auth_layout_component__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__shared_shared_module__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angular_2_local_storage__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_angular_2_local_storage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_angular_2_local_storage__);
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__["a" /* TranslateStaticLoader */](http, './assets/i18n', '.json');
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_11__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
            __WEBPACK_IMPORTED_MODULE_12__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_14_angular_2_local_storage__["LocalStorageModule"].withConfig({
                prefix: 'my-app',
                storageType: 'localStorage'
            }),
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
            __WEBPACK_IMPORTED_MODULE_13__shared_shared_module__["a" /* SharedModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__app_routing__["a" /* AppRoutes */]),
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__["b" /* TranslateModule */].forRoot({
                provide: __WEBPACK_IMPORTED_MODULE_6_ng2_translate_ng2_translate__["c" /* TranslateLoader */],
                useFactory: (createTranslateLoader),
                deps: [__WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */]]
            }),
            __WEBPACK_IMPORTED_MODULE_7__angular_material__["a" /* MaterialModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_flex_layout__["a" /* FlexLayoutModule */],
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__ = __webpack_require__(135);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutes; });


var AppRoutes = [{
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_0__layouts_admin_admin_layout_component__["a" /* AdminLayoutComponent */],
        children: [{
                path: 'reflectors',
                loadChildren: './zComponents/reflectors/reflectors.module#ReflectorsModule'
            },
            {
                path: 'tags',
                loadChildren: './zComponents/tags/tags.module#TagsModule'
            }, {
                path: 'telebot-settings',
                loadChildren: './zComponents/telebot/telebot.module#TelebotModule'
            }, {
                path: '',
                loadChildren: './zComponents/puzzles/puzzles.module#PuzzlesModule'
            }]
    }, {
        path: '',
        component: __WEBPACK_IMPORTED_MODULE_1__layouts_auth_auth_layout_component__["a" /* AuthLayoutComponent */],
        children: [{
                path: 'session',
                loadChildren: './session/session.module#SessionModule'
            }]
    }, {
        path: '**',
        redirectTo: ''
    }];
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__(137);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccordionAnchorDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AccordionAnchorDirective = (function () {
    function AccordionAnchorDirective(navlink) {
        this.navlink = navlink;
    }
    AccordionAnchorDirective.prototype.onClick = function (e) {
        this.navlink.toggle();
    };
    return AccordionAnchorDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AccordionAnchorDirective.prototype, "onClick", null);
AccordionAnchorDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[appAccordionToggle]'
    }),
    __param(0, __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"])(__WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */])),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a" /* AccordionLinkDirective */]) === "function" && _a || Object])
], AccordionAnchorDirective);

var _a;
//# sourceMappingURL=accordionanchor.directive.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__ = __webpack_require__(244);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__accordionanchor_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__ = __webpack_require__(137);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__accordionlink_directive__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion_directive__ = __webpack_require__(136);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__accordion_directive__["a"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ 246:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_screenfull___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_screenfull__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToggleFullscreenDirective; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToggleFullscreenDirective = (function () {
    function ToggleFullscreenDirective() {
    }
    ToggleFullscreenDirective.prototype.onClick = function () {
        if (__WEBPACK_IMPORTED_MODULE_1_screenfull__["enabled"]) {
            __WEBPACK_IMPORTED_MODULE_1_screenfull__["toggle"]();
        }
    };
    return ToggleFullscreenDirective;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ToggleFullscreenDirective.prototype, "onClick", null);
ToggleFullscreenDirective = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[appToggleFullscreen]'
    })
], ToggleFullscreenDirective);

//# sourceMappingURL=toggle-fullscreen.directive.js.map

/***/ }),

/***/ 247:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accordion__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__ = __webpack_require__(246);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */],
            __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionLinkDirective */],
            __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]
        ],
        exports: [
            __WEBPACK_IMPORTED_MODULE_2__accordion__["a" /* AccordionAnchorDirective */],
            __WEBPACK_IMPORTED_MODULE_2__accordion__["b" /* AccordionLinkDirective */],
            __WEBPACK_IMPORTED_MODULE_2__accordion__["c" /* AccordionDirective */],
            __WEBPACK_IMPORTED_MODULE_3__fullscreen_toggle_fullscreen_directive__["a" /* ToggleFullscreenDirective */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_1__menu_items_menu_items__["a" /* MenuItems */]]
    })
], SharedModule);

//# sourceMappingURL=shared.module.js.map

/***/ }),

/***/ 248:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 442:
/***/ (function(module, exports) {

module.exports = "<div class=\"app\" #root=\"$implicit\" dir=\"ltr\" [ngClass]=\"{'app-dark': dark, 'boxed': boxed, 'collapsed-sidebar': collapseSidebar, 'compact-sidebar': compactSidebar}\">\n  <md-toolbar class=\"main-header\" color=\"primary\">\n    <button (click)=\"sidemenu.toggle()\" md-icon-button>\n      <md-icon>menu</md-icon>\n    </button>\n    <div class=\"branding\">\n      <div style=\"font-size: 32px;font-weight: 600;\">MIRROR</div>\n    </div>\n    <div ng class=\"search-bar\" fxFlex>\n      <!--<form class=\"search-form\" fxShow=\"false\" fxShow.gt-xs>\n        <md-icon>search</md-icon>\n        <input type=\"text\" placeholder=\"Search\" autofocus=\"true\" />\n      </form>-->\n    </div>\n    <!--<button (click)=\"end.toggle()\" md-icon-button class=\"ml-xs overflow-visible\">\n      <md-icon>notifications</md-icon>\n      <span class=\"notification-label\">5</span>\n    </button>-->\n    <button appToggleFullscreen md-icon-button>\n      <md-icon>fullscreen</md-icon>\n    </button>\n    <button [md-menu-trigger-for]=\"user\" md-icon-button class=\"ml-xs\">\n      <md-icon>person</md-icon>\n    </button>\n    \n    <md-menu #user=\"mdMenu\" x-position=\"before\">\n      <!--<button md-menu-item>\n        <md-icon>settings</md-icon>\n        Settings\n      </button>\n      <button md-menu-item>\n        <md-icon>account_box</md-icon>\n        Profile\n      </button>\n      <button md-menu-item>\n        <md-icon>notifications_off</md-icon>\n        Disable notifications\n      </button>-->\n      <button md-menu-item>\n        <md-icon>exit_to_app</md-icon>\n        Sign Out\n      </button>\n    </md-menu>\n  </md-toolbar>\n\n  <md-sidenav-container class=\"app-inner\">\n    <md-sidenav #sidemenu class=\"sidebar-panel\" id=\"sidebar-panel\" [mode]=\"isOver() ? 'over' : 'side'\" [opened]=\"!isOver()\" (mouseover)=\"menuMouseOver()\" (mouseout)=\"menuMouseOut()\">\n      <md-nav-list appAccordion class=\"navigation\">\n        <md-list-item appAccordionLink *ngFor=\"let menuitem of menuItems.getAll()\" group=\"{{menuitem.state}}\">\n          <a appAccordionToggle class=\"relative\" md-ripple [routerLink]=\"['/', menuitem.state]\" *ngIf=\"menuitem.type === 'link'\">\n            <md-icon>{{ menuitem.icon }}</md-icon>\n            <span>{{ menuitem.name | translate }}</span>\n            <span fxFlex></span>\n            <span class=\"menu-badge mat-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a appAccordionToggle class=\"relative\" md-ripple href=\"{{menuitem.state}}\" *ngIf=\"menuitem.type === 'extLink'\">\n            <md-icon>{{ menuitem.icon }}</md-icon>\n            <span>{{ menuitem.name | translate }}</span>\n            <span fxFlex></span>\n            <span class=\"menu-badge mat-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a appAccordionToggle class=\"relative\" md-ripple href=\"{{menuitem.state}}\" target=\"_blank\" *ngIf=\"menuitem.type === 'extTabLink'\">\n            <md-icon>{{ menuitem.icon }}</md-icon>\n            <span>{{ menuitem.name | translate }}</span>\n            <span fxFlex></span>\n            <span class=\"menu-badge mat-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n          </a>\n          <a appAccordionToggle class=\"relative\" md-ripple href=\"javascript:;\" *ngIf=\"menuitem.type === 'sub'\">\n            <md-icon>{{ menuitem.icon }}</md-icon>\n            <span>{{ menuitem.name | translate }}</span>\n            <span fxFlex></span>\n            <span class=\"menu-badge mat-{{ badge.type }}\" *ngFor=\"let badge of menuitem.badge\">{{ badge.value }}</span>\n            <md-icon class=\"menu-caret\">arrow_drop_down</md-icon>\n          </a>\n          <md-nav-list class=\"sub-menu\" *ngIf=\"menuitem.type === 'sub'\">\n            <md-list-item *ngFor=\"let childitem of menuitem.children\" routerLinkActive=\"open\">\n              <a [routerLink]=\"['/', menuitem.state, childitem.state ]\" class=\"relative\" md-ripple>{{ childitem.name | translate }}</a>\n            </md-list-item>\n          </md-nav-list>\n        </md-list-item>\n      </md-nav-list>\n    </md-sidenav>\n    <md-sidenav #end align=\"end\" class=\"chat-panel\" mode=\"over\" opened=\"false\">\n      <md-tab-group [selectedIndex]=\"1\" md-stretch-tabs>\n        <md-tab>\n          <ng-template md-tab-label>Today</ng-template>\n          <div class=\"scroll\">\n            <md-list class=\"pt-1 pb-1\">\n              <md-list-item>\n                <div fxLayout=\"column\">\n                  <h2 class=\"ma-0\">{{ today | date:'EEEE' }}</h2>\n                  <h6 class=\"mat-text-muted ma-0\"><span>{{ today | date:'dd' }}</span>&nbsp;<span>{{ today | date:'MMMM' }}</span></h6>\n                </div>\n              </md-list-item>\n            </md-list>\n            <md-nav-list>\n              <md-divider></md-divider>\n              <h3 md-subheader class=\"text-uppercase font-weight-bold\">Stocks</h3>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">NASDAQ</a>\n                 <span class=\"mat-text-muted text-md mr-xs ml-xs\">4,492.87</span>\n                 <span>-0.29%</span>\n              </md-list-item>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">NYSE</a>\n                 <span class=\"mat-text-muted text-md mr-xs ml-xs\">10,692.07</span>\n                 <span>-0.53%</span>\n              </md-list-item>\n              <md-list-item>\n                 <a md-line href=javascript:;>DOW J</a>\n                 <span class=\"mat-text-muted text-md mr-xs ml-xs\">17,046.81</span>\n                 <span>-0.14%</span>\n              </md-list-item>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">APPL</a>\n                 <span class=\"mat-text-muted text-md mr-xs ml-xs\">100,89</span>\n                 <span>+0.75%</span>\n              </md-list-item>\n              <md-divider></md-divider>\n              <h3 md-subheader class=\"text-uppercase font-weight-bold\">Weather</h3>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">{{ today | date:'shortTime' }}</a>\n                 <p md-line class=\"mat-text-muted\">London</p>\n                 <span class=\"h4 pe-is-w-blizzard mr-1\"></span>\n                 <span class=\"h4\">26°</span>\n              </md-list-item>\n              <md-divider></md-divider>\n              <h3 md-subheader class=\"text-uppercase font-weight-bold\">Todo</h3>\n              <md-list-item>\n                <a md-line href=\"javascript:;\">Learn Angular 2.0</a>\n                <p md-line class=\"mat-text-muted text-md\">2:45PM</p>\n              </md-list-item>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">Learn Angular Material</a>\n                 <p md-line class=\"mat-text-muted text-md\">3:20PM</p>\n              </md-list-item>\n              <md-list-item>\n                 <a md-line href=\"javascript:;\">Write documentation</a>\n                 <p md-line class=\"mat-text-muted text-md\">6:00PM</p>\n              </md-list-item>\n              <md-divider class=\"mt-xs mb-xs\"></md-divider>\n              <h3 md-subheader class=\"text-uppercase font-weight-bold\">Stats</h3>\n              <md-list-item>\n                <p class=\"text-md\" md-line>Local Storage (4023 / 10690)</p>\n                <md-progress-bar  md-line mode=\"determinate\" color=\"warn\" value=\"40\"></md-progress-bar>\n              </md-list-item>\n              <md-list-item>\n                <p class=\"text-md\" md-line>Cloud Storage (700 / 1030)</p>\n                <md-progress-bar  md-line mode=\"determinate\" color=\"accent\" value=\"70\"></md-progress-bar>\n              </md-list-item>\n              <md-list-item>\n                <p class=\"text-md\" md-line>Local Storage (20 / 100)</p>\n                <md-progress-bar  md-line mode=\"determinate\" value=\"20\"></md-progress-bar>\n              </md-list-item>\n            </md-nav-list>\n          </div>\n        </md-tab>\n        <md-tab>\n          <ng-template md-tab-label>Notifications</ng-template>\n          <div class=\"scroll\">\n            <md-nav-list>\n              <md-list-item>\n                <md-icon md-list-avatar class=\"mat-text-primary\">people</md-icon>\n                <h4 md-line>Social</h4>\n                <p md-line>Ligula Purus Adipiscing</p>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar class=\"mat-text-warn\">local_offer</md-icon>\n                <h4 md-line>Promotions</h4>\n                <p md-line>Etiam Ligula Dapibus</p>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar class=\"mat-text-accent\">info</md-icon>\n                <h4 md-line>Updates</h4>\n                <p md-line>Sollicitudin Euismod Fringilla</p>\n              </md-list-item>\n\n              <md-list-item>\n                <md-icon md-list-avatar class=\"mat-indigo\">delete_sweep</md-icon>\n                <h4 md-line>Removed 6 items from task list</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1427207139000 | date: 'fullDate' }}</span>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar>check_circle</md-icon>\n                <h4 md-line>Completed 2 projects</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1427412725000 | date: 'fullDate' }}</span>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar>notifications_paused</md-icon>\n                <h4 md-line>Muted notifications</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1427546580000 | date: 'fullDate' }}</span>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar>person_add</md-icon>\n                <h4 md-line>Added Joel to contact list</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1428275520000 | date: 'fullDate' }}</span>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar>phone_missed</md-icon>\n                <h4 md-line>Missed live call from Ellie</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1428830580000 | date: 'fullDate' }}</span>\n              </md-list-item>\n              <md-list-item>\n                <md-icon md-list-avatar>group_add</md-icon>\n                <h4 md-line>You've been added to HR group</h4>\n                <span class=\"text-md mat-text-muted\" md-line>{{ 1429363920000 | date: 'fullDate' }}</span>\n              </md-list-item>\n            </md-nav-list>\n          </div>\n        </md-tab>\n      </md-tab-group>\n    </md-sidenav>\n    <router-outlet></router-outlet>\n  </md-sidenav-container>\n\n  <!-- Demo Purposes Only -->\n  <!--<button md-fab color=\"warn\" class=\"mat-fab-bottom-right\" (click)=\"showSettings = true\">\n    <md-icon class=\"md-24\">settings</md-icon>\n  </button>-->\n  <md-card class=\"settings-panel\" *ngIf=\"showSettings\">\n    <md-toolbar color=\"warn\">\n      <span fxFlex>Options</span>\n      <button md-icon-button (click)=\"closeSettings()\">\n        <md-icon>close</md-icon>\n      </button>\n    </md-toolbar>\n    <md-card-content class=\"demo-checkbox\">\n      <div class=\"pb-1\">\n        <md-checkbox [(ngModel)]=\"collapseSidebar\" (change)=\"compactSidebar = false\" [align]=\"end\">Collapsed Sidebar</md-checkbox>\n      </div>\n      <div class=\"pb-1\">\n        <md-checkbox [(ngModel)]=\"compactSidebar\" (change)=\"collapseSidebar = false\" [align]=\"end\">Compact Sidebar</md-checkbox>\n      </div>\n      <div class=\"pb-1\">\n        <md-checkbox [(ngModel)]=\"boxed\" [align]=\"end\">Boxed Layout</md-checkbox>\n      </div>\n      <div class=\"pb-1\">\n        <md-checkbox [(ngModel)]=\"dark\" [align]=\"end\">Dark Mode</md-checkbox>\n      </div>\n    </md-card-content>\n  </md-card>\n  <!-- /Demo Purposes Only -->\n\n</div>"

/***/ }),

/***/ 443:
/***/ (function(module, exports) {

module.exports = "<md-sidenav-container>\n  <router-outlet></router-outlet>\n</md-sidenav-container>"

/***/ }),

/***/ 510:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(201);


/***/ })

},[510]);
//# sourceMappingURL=main.bundle.js.map